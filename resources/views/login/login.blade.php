<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{-- <script src="https://cdn.tailwindcss.com"></script> --}}
    @vite('resources/css/app.css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.css">

    <title>Login</title>
</head>

<body>

    <div class="grid items-center w-screen h-screen">
        <div class="w-full grid gap-20">
            <div class="mx-auto">
                <img src="{{ asset('images/icon/Group 48.png') }}" alt="">
            </div>
            <form action="{{ route('login') }}" method="POST" class="mx-auto">
                @csrf
                <div class="grid gap-10">
                    <div class="grid gap-5 ">
                        <div class="flex">
                            <img src="{{ asset('images/icon/userlogin.svg') }}" alt="" class="translate-x-7">
                            <input type="text" name="username" placeholder="USERNAME"
                                class="p-2 pl-10 rounded border border-black">
                        </div>
                        <div class="flex">
                            <img src="{{ asset('images/icon/lock.svg') }}" alt="" class="translate-x-7">
                            <input type="password" name="username" placeholder="USERNAME"
                                class="p-2 pl-10 rounded border border-black">
                        </div>
                        
                    </div>
                    <div class="grid gap-2">
                        <div class=" bg-sky-600 mx-auto rounded w-3/4 py-3">
                            <button type="submit" name="btn-login" id="btn-login" class="w-full"><span class="text-center text-white text-bold text-xl">LOGIN</span>
                        </div>
                        <div class="flex justify-between mx-auto w-3/4">
                            <a href="{{ route('register') }}"><span>Đăng ký</span></a>
                            <a href=""><span>Quên mật khẩu</span></a>
                        </div>
                    </div>
                    
                </div>
            </form>
        </div>

    </div>


    @include('layout.script')

</body>

</html>
