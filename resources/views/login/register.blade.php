<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{-- <script src="https://cdn.tailwindcss.com"></script> --}}
    @vite('resources/css/app.css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.css">

    <title>Login</title>
</head>

<body>

    <div class="container mx-auto bg-slate-400 h-screen">
        <div class="w-full h-full flex max-[640px]:grid max-[640px]:flex max-[640px]:w-full max-[640px]:h-1/2  items-center">
            <div class="w-1/2">
                <div class="mx-auto w-1/2">
                    <img class="mx-auto w-1/2" src="{{ asset('images/icon/Group 48.png') }}" alt="">
                </div>
            </div>    
            <div >
                <div class="grid gap-20">
                    <div class="text-white text-3xl">
                        <p>Create an account</p>
                    </div>
                    <form action="{{ route('registerUser') }}" method="POST">
                        <div>
                            <input type="text" name="" id="">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
    </div>

    @include('layout.script')

</body>

</html>
