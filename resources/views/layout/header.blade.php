<header>
    <div class="flex items-center justify-between w-full px-3 py-2 header bg-stone-100 lg:grid lg:grid-cols-5 lg:px-8 lg:py-8 lg:gap-10">
        <div class="flex items-center gap-4 p-1 logo lg:w-full lg:col-span-1">
            <button class="lg:hidden" onclick="showModal()"><img src="./images/icon/menu_black_24dp (2) 1.svg"
                    alt=""></button>
            <a href="#"><img src="./images/icon/Group 2 1.svg" alt="logo"></a>
            <h1 class="hidden text-blue-600 lg:text-lg lg:block">Aster News</h1>
        </div>
        <div class="flex items-center justify-between gap-10 lg:col-span-3 lg:w-full lg:ml-0">
            <div class="lg:justify-between lg:items-center lg:flex lg:w-full">
                <div class="lg:flex lg:w-3/5">
                    <input type="search" name="q"
                        class="hidden w-full py-2 pl-10 text-lg text-white bg-blue-100 rounded-md lg:block lg:h-14 focus:outline-none focus:bg-white focus:text-gray-900"
                        placeholder="Search for news.." autocomplete="off">
                    <button type="submit"
                        class="ml-20 lg:ml-0 lg:-translate-x-10 lg:p-1 lg:focus:outline-none lg:focus:shadow-outline">
                        <img src="./images/icon/search.svg" alt="">
                    </button>
                </div>
                <div class="hidden w-2/5 border-2 rounded lg:justify-between lg:flex lg:px-6 lg:py-4 lg:block">
                    <button type="submit flex">
                        <span>Latest news on <span class="text-blue-400">
                                Covid-19</span></span>
                    </button>
                    <img src="./images/icon/arrow-right.svg" alt="">
                </div>
            </div>
        </div>
        <div class="lg:w-full lg:col-span-1">
            <div class="text-right">
                <div class="inline-flex items-center gap-4">
                    <img src="./images/icon/user.svg" alt="">
                    <p class="lg:ml-4 lg:mr-6">My Profile</p>
                    <img class="hidden lg:block" src="./images/icon/Group 9.svg" alt="">
                </div>
            </div>
        </div>
    </div>
</header>