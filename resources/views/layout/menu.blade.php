<div id="overlay" class="fixed inset-0 z-10 hidden w-screen h-screen bg-black bg-opacity-60" onclick="off()"></div>
<div class="grid col-span-3 p-4 lg:py-6 lg:p-2 gap-y-4">
    <div class="grid w-full gap-y-4">
        <div class="w-full ">
            <p class="text-bold lg:text-xl">Top Stories for you</p>
        </div>
        <div class="flex items-center lg:text-center justify-between lg:pr-10 lg:items-center gap-5 lg:flex lg:w-full">
            <div class="max-w-xs lg:items-center lg:flex sm:max-w-xl lg:max-w-full lg:gap-4 menu-nav lg:max-w-5xl">
                <a href="#">
                    <div class="py-1 m-2 mt-auto text-center bg-blue-400 rounded-3xl lg:px-4">All</div>
                </a>
                <a href="{{ route('category.android', ['id' => '1']) }}">
                    <div class="py-1 m-2 mt-auto text-center bg-white rounded-3xl lg:px-4">Android</div>
                </a>
                <a href="#">
                    <div class="px-4 py-1 m-2 mt-auto text-center bg-white rounded-3xl lg:px-4">Cricket</div>
                </a>
                <a href="#">
                    <div class="px-4 py-1 m-2 mt-auto text-center bg-white rounded-3xl lg:px-4">iPhone</div>
                </a>
                <a href="#">
                    <div class="px-4 py-1 m-2 mt-auto text-center bg-white rounded-3xl lg:px-4">Google</div>
                </a>
                <a href="#">
                    <div class="py-1 m-2 mt-auto text-center bg-white rounded-3xl lg:px-4">Nano Technology
                    </div>
                </a>
                <a href="#">
                    <div class="py-1 m-2 mt-auto text-center bg-white lg:px-4 rounded-3xl">Mental Health</div>
                </a>
            </div>
            <button class="" onclick="showCategory()"><i class="fa-solid fa-ellipsis"></i></button>
        </div>
        <div class="p-4 grid gap-y-5  bg-white rounded hidden" id="category">
            <div class="flex justify-between items-center px-3 py-2 lg:px-6 lg:py-4 border-b">
                <div class="text-bold text-2xl">
                    <p>Tất cả chủ đề</p>
                </div>
                <button onclick="closeCategory()"><i class="fa-solid fa-xmark"></i></button>
            </div>
            <div class="grid grid-cols-2 lg:grid-cols-4 gap-2 lg:gap-4 px-2 lg:px-4 py-2 lg:py-3">

                <div class="grid gap-y-2 lg:gap-y-4 text-center">
                    <a href=""><span>Thế giới</span></a>
                    <a href=""><span>Thời sự</span></a>
                    <a href=""><span>Kinh doanh</span></a>
                    <a href=""><span>U23 Châu Á</span></a>
                    <a href=""><span>Virus Corona</span></a>
                    <a href=""><span>Tư vấn online</span></a>
                    <a href=""><span>APEC</span></a>
                    <a href=""><span>Việc làm</span></a>
                </div>
                <div class="grid gap-y-2 lg:gap-y-4 text-center">
                    <a href=""><span>Khoa học</span></a>
                    <a href=""><span>Pháp luật</span></a>
                    <a href=""><span>Đời sống</span></a>
                    <a href=""><span>Du lịch</span></a>
                    <a href=""><span>Bóng đá</span></a>
                    <a href=""><span>Thể thao</span></a>
                    <a href=""><span>Năng lượng xanh</span></a>
                    <a href=""><span>Tái định cư</span></a>
                </div>
                <div class="grid gap-y-2 lg:gap-y-4 text-center">
                    <a href=""><span>Sea Game</span></a>
                    <a href=""><span>Giao thông</span></a>
                    <a href=""><span>Quân sự</span></a>
                    <a href=""><span>Giải trí</span></a>
                    <a href=""><span>Chứng khoán</span></a>
                    <a href=""><span>Bất động sản</span></a>
                    <a href=""><span>Sáng kiến khoa học</span></a>
                </div>
                <div class="grid gap-y-2 lg:gap-y-4 text-center">
                    <a href=""><span>Hẹn hò</span></a>
                    <a href=""><span>Sức khoẻ</span></a>
                    <a href=""><span>Xe</span></a>
                    <a href=""><span>Số hoá</span></a>
                    <a href=""><span>Blockchain</span></a>
                    <a href=""><span>Ẩm thực</span></a>
                    <a href=""><span>Tiêu dùng</span></a>
                    <a href=""><span>Video</span></a>
                </div>
            </div>
        </div>
    </div>
