    <div class="hidden w-full col-span-1 px-4 lg:pr-1 lg:block">
        <div class="hidden bg-white rounded shadow-2xl lg:p-4 lg:p-1 lg:mb-5 lg:mb-2 lg:block">
            <div class="inline-flex items-center border-b lg:py-2 lg:mb-4">
                <p class="lg:mr-20 lg:w-auto">Coimbatore, Tamil Nadu</p>
                <div class="float-right">
                    <img src="./images/icon/crosshair.svg" alt="">
                </div>
            </div>
            <div class="inline-flex items-center justify-between w-full">
                <div>
                    <p class="lg:text-base lg:mb-2">Sunny</p>
                    <div class="font-bold lg:text-2xl lg:mb-4 ">31&deg;c</div>
                    <div class="flex lg:gap-5 lg:gap-1">
                        <p>Celsius</p>
                        <p class="opacity-30">Fahrenheit</p>
                    </div>
                </div>
                <div class="">
                    <img class="object-cover w-auto" src="./images/icon/sun.svg" alt="">
                </div>
            </div>
        </div>

        <div class="hidden bg-white rounded shadow-2xl lg:p-4 lg:p-1 lg:mb-5 lg:mb-2 lg:block">
            <div class="flex items-center lg:mb-5 lg:mb-2">
                <img class="lg:mr-4" src="./images/icon/feather.svg" alt="">
                <p class="lg:text-lg">Become a Story Writer</p>
            </div>
            <div class="flex items-center justify-between">
                <p class="w-3/5 text-left opacity-60 lg:text-lg">Contribute
                    stories and start earning.</p>
                <div class="text-center border rounded lg:py-2 lg:px-3">
                    <a href=""><span class="text-blue-400 lg:text-lg ">Know
                            More</span></a>
                </div>
            </div>
        </div>

        <div class="hidden bg-white rounded shadow-2xl lg:p-4 lg:p-1 lg:mb-5 lg:mb-2 lg:block">
            <div class="flex items-center justify-between border-b lg:pb-2 lg:mb-4">
                <div class="flex items-center lg:w-3/5">
                    <img class="lg:mr-5" src="./images/icon/file-text.svg" alt="">
                    <p class="text-center">Quick Bytes</p>
                </div>
                <div class="flex items-center justify-between w-1/3">
                    <div class="bg-blue-100 rounded-full lg:p-4 lg:p-1 ">
                        <img src="./images/icon/arrow_1.svg" alt="">
                    </div>
                    <div class="bg-blue-600 rounded-full lg:p-4 lg:p-1">
                        <img src="./images/icon/arrow_2.svg" alt="">
                    </div>
                </div>
            </div>
            <div class="mb-5 opacity-60">
                <p>The price of petrol remained unchanged on July 6
                    after reaching a new record high on the previous
                    day, according to a price notification by
                    state-owned fuel retailers. The diesel price
                    remained stable for the second consecutive day.
                    <br>
                    <br>
                    The increase on July 5, 35th in two months, took the
                    petrol price in Delhi closer to Rs 100 per
                    litre-mark. The petrol price in the national capital
                    soared to Rs 99.9 a litre and diesel was priced at
                    Rs 89.4 per litre, according to Bharat Petroleum's
                    price listing.
                </p>
            </div>
            <div class="w-full mb-2 text-center">
                <img class="object-cover w-auto mx-auto" src="./images/icon/Group 37.svg" alt="">
            </div>
        </div>

        <div class="grid px-10 py-3 bg-white rounded shadow-2xl lg:p-4 lg:p-1 lg:mb-5 lg:mb-2 gap-y-2">
            <div class="flex items-center lg:mb-5 lg:mb-2">
                <p class="lg:text-lg">Subscribe to our
                    newsletter</p>
            </div>
            <div class="flex items-center justify-between lg:mb-5 lg:mb-2">
                <input class="w-full px-4 py-2 rounded bg-slate-200 lg:h-10" type="email" placeholder=" Enter Email">
            </div>
            <div class="items-center justify-between bg-blue-400 rounded flext lg:py-5">
                <a class="w-full " href="#">
                    <p class="py-2 text-center text-white">Subscribe</p>
                </a>
            </div>
        </div>

    </div>
    </div>