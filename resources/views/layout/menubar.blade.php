<div class="lg:grid lg:grid-cols-5 lg:gap-10 lg:block">
    <div class="absolute top-0 left-0 z-20 hidden w-4/5 col-span-1 lg:static bg-slate-200 lg:w-full lg:bg-transparent lg:block"
        id="modal">
        <div class="container justify-between">
            <div class="flex items-center px-6 py-4 lg:py-6 lg:px-8 lg:hidden lg:mb-2">
                <a href="#"><img src="./images/icon/Group 2 1.svg" alt="logo"></a>
                <h1 class="ml-4 text-blue-600 lg:text-lg">Aster News</h1>
            </div>
            <div
                class="inline-flex items-center w-11/12 gap-3 p-1 px-3 py-4 bg-blue-100 border-t rounded-r-full lg:gap-3 lg:px-4 lg:py-4 lg:mb-2">
                <div class="my-auto lg:my-auto">
                    <div class="px-0.5 py-0.5 bg-blue-600 rounded-full lg:py-1 lg:px-1"></div>
                </div>
                <a href="#"><img src="./images/icon/home.svg" alt=""></a>
                <p class="ml-2 text-blue-400 lg:text-lg">Top Stories</p>
            </div>
            <div class="flex items-center gap-5 p-1 px-6 py-4 lg:py-5 lg:px-8 lg:mb-2">
                <a href="#"><img src="./images/icon/globe.svg" alt=""></a>
                <p class="lg:ml-5 lg:ml-2 lg:text-lg">Around the World</p>
            </div>
            <div class="flex items-center gap-5 p-1 px-6 py-4 lg:py-5 lg:px-8 lg:mb-2">
                <a href="#"><img src="./images/icon/briefcase.svg" alt=""></a>
                <p class=" lg:ml-5 lg:ml-2 lg:text-lg">Business</p>
            </div>
            <div class="flex items-center gap-5 p-1 px-6 py-4 lg:py-5 lg:px-8 lg:mb-2">
                <a href="#"><img src="./images/icon/activity.svg" alt=""></a>
                <p class="lg:ml-5 lg:ml-2 lg:text-lg">Health</p>
            </div>
            <div class="flex w-11/12 gap-5 px-6 py-4 border-t border-b lg:py-9 lg:px-6 lg:mb-2">
                <a href="#"><img src="./images/icon/shield.svg" alt=""></a>
                <p class="lg:ml-5 lg:ml-2 lg:text-lg">Covid 19</p>
            </div>
            <div class="flex gap-5 p-1 px-6 py-4 lg:py-5 lg:px-8 lg:mb-2">
                <a href="#"><img src="./images/icon/play-circle.svg" alt=""></a>
                <p class="lg:ml-5 lg:ml-2 lg:text-lg">Entertainment</p>
            </div>
            <div class="flex gap-5 px-6 py-4 lg:py-5 lg:px-8 lg:p-1 lg:mb-2">
                <a href="#"><img src="./images/icon/award.svg" alt=""></a>
                <p class="lg:ml-5 lg:ml-2 lg:text-lg">Sports</p>
            </div>
            <div class="flex gap-5 px-6 py-4 lg:py-5 lg:px-8 lg:p-1 lg:mb-2">
                <a href="#"><img src="./images/icon/message-circle.svg" alt=""></a>
                <p class="lg:ml-5 lg:ml-2 lg:text-lg">Discussion</p>
            </div>
            <div class="flex gap-5 px-6 py-4 lg:py-5 lg:px-8 lg:p-1 lg:mb-2">
                <a href="#"><img src="./images/icon/bell.svg" alt=""></a>
                <p class="lg:ml-5 lg:ml-2 lg:text-lg">Notification</p>
            </div>
            <div class="flex gap-5 px-6 py-4 lg:py-5 lg:px-8 lg:mb-8 lg:p-1 lg:mb-2">
                <a href="#"><img src="./images/icon/settings.svg" alt=""></a>
                <p class="lg:ml-5 lg:ml-2 lg:text-lg">News Feed Settings</p>
            </div>

            <div class="items-center p-4 m-4 bg-blue-400 rounded lg:mx-auto lg:my-3 lg:w-11/12">
                <div class="px-4 py-2 mx-auto lg:py-5 lg:px-9 lg:p-1">
                    <div class="flex justify-between mb-5 ">
                        <img src="./images/icon/gift.svg" alt="">
                        <p class="ml-4 text-lg text-white">Subscribe to
                            Premium</p>
                    </div>
                    <div class="flex items-center justify-between">
                        <div class="flex">
                            <p class="text-3xl text-white">$8</p>
                            <p class="pt-3 text-lg text-white lg:mr-1">/m</p>
                        </div>

                        <a href="#">
                            <p
                                class="px-4 py-2 text-base text-white bg-blue-600 rounded lg:pt-2 lg:p-1 lg:pl-7 lg:pb-2 lg:pr-7 lg:text-lg">
                                Upgrade</p>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
