@extends('layout.master')

@section('title', 'Category')

@section('content')

    <div class="grid lg:mb-10 lg:mb-4 gap-y-4">
        <div class="grid gap-2 lg:gap-4 p-2 lg:p-4">
            <div class="p-2 inline-flex items-center lg:place-items-start gap-5 lg:gap-10">
                
                    <img src="{{ asset('images/Rectangle 16.png') }}" alt="">
                
                
                <div class="grid gap-y-2 h-full">
                    <p class="text-bold text-lg">Samsung Galaxy F22 launched in India: Price, features, other details</p>
                    <p class="opacity-60">Samsung Galaxy F22 has been launched in India. The new smartphone has been priced
                        in the mid-range segment. The new smartphone is powered by a MediaTek chipset and features a high
                        refresh rate AMOLED display.</p>
                </div>
            </div>
            <div class="footer flex items-center justify-between px-4">
                <div class="flex  gap-2 lg:gap-10">
                    <p class="opacity-60">The Mint</p>
                    <p class="opacity-60">15 mins ago</p>
                </div>
                <div class="flex gap-2 lg:gap-10">
                    <div class="flex gap-2">
                        <i class="fa-solid fa-arrow-up-from-bracket text-sky-600"></i>
                        <p class=" text-xs text-sky-600 lg:block">Share</p>
                    </div>
                    <div class="flex gap-2">
                        <i class="fa-solid fa-bookmark text-sky-600"></i>
                        <p class=" text-xs text-sky-600 lg:block">Read Late</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid gap-2 lg:gap-4 p-2 lg:p-4">
            <div class="p-2 inline-flex items-center  lg:place-items-start  gap-5 lg:gap-10">
                <img class="w-auto items-center" src="{{ asset('images/Rectangle 16.png') }}" alt="">
                <div class="grid gap-y-2">
                    <p class="text-bold text-lg">Battlegrounds Mobile India iOS release date</p>
                    <p class="opacity-60">The reason behind their disappointment is that iPhone users have been..
                    </p>
                </div>
            </div>
            <div class="footer flex items-center justify-between px-4">
                <div class="flex  gap-2 lg:gap-10">
                    <p class="opacity-60">The Mint</p>
                    <p class="opacity-60">15 mins ago</p>
                </div>
                <div class="flex gap-2 lg:gap-10">
                    <div class="flex gap-2">
                        <i class="fa-solid fa-arrow-up-from-bracket text-sky-600"></i>
                        <p class=" text-xs text-sky-600 lg:block">Share</p>
                    </div>
                    <div class="flex gap-2">
                        <i class="fa-solid fa-bookmark text-sky-600"></i>
                        <p class=" text-xs text-sky-600 lg:block">Read Late</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid gap-2 lg:gap-4 p-2 lg:p-4">
            <div class="p-2 inline-flex items-center  lg:place-items-start  gap-5 lg:gap-10">
                <img class="w-auto items-center" src="{{ asset('images/Rectangle 16.png') }}" alt="">
                <div class="grid gap-y-2">
                    <p class="text-bold text-lg">Instagram could be jumping on the paid subscription..</p>
                    <p class="opacity-60">The reason behind their disappointment is that iPhone users have been..
                    </p>
                </div>
            </div>
            <div class="footer flex items-center justify-between px-4">
                <div class="flex  gap-2 lg:gap-10">
                    <p class="opacity-60">The Mint</p>
                    <p class="opacity-60">15 mins ago</p>
                </div>
                <div class="flex gap-2 lg:gap-10">
                    <div class="flex gap-2">
                        <i class="fa-solid fa-arrow-up-from-bracket text-sky-600"></i>
                        <p class=" text-xs text-sky-600 lg:block">Share</p>
                    </div>
                    <div class="flex gap-2">
                        <i class="fa-solid fa-bookmark text-sky-600"></i>
                        <p class=" text-xs text-sky-600 lg:block">Read Late</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid gap-2 lg:gap-4 p-2 lg:p-4">
            <div class="p-2 inline-flex items-center  lg:place-items-start  gap-5 lg:gap-10">
                <img class="w-auto items-center" src="{{ asset('images/Rectangle 16.png') }}" alt="">
                <div class="grid gap-y-2">
                    <p class="text-bold text-lg">Petrol, diesel prices -  July 6: Fuel prices unchanged after touching record high</p>
                    <p class="opacity-60">The price of petrol remained unchanged on July 6 after reaching a new record high on the previous day, according to a price notification by state-owned..</p>
                </div>
            </div>
            <div class="footer flex items-center justify-between px-4">
                <div class="flex  gap-2 lg:gap-10">
                    <p class="opacity-60">The Mint</p>
                    <p class="opacity-60">15 mins ago</p>
                </div>
                <div class="flex gap-2 lg:gap-10">
                    <div class="flex gap-2">
                        <i class="fa-solid fa-arrow-up-from-bracket text-sky-600"></i>
                        <p class=" text-xs text-sky-600 lg:block">Share</p>
                    </div>
                    <div class="flex gap-2">
                        <i class="fa-solid fa-bookmark text-sky-600"></i>
                        <p class=" text-xs text-sky-600 lg:block">Read Late</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid gap-2 lg:gap-4 p-2 lg:p-4">
            <div class="p-2 inline-flex items-center  lg:place-items-start  gap-5 lg:gap-10">
                <img class="w-auto items-center" src="{{ asset('images/Rectangle 16.png') }}" alt="">
                <div class="grid gap-y-2">
                    <p class="text-bold text-lg">Android smartphone users alert! Remove these 9 apps now</p>
                    <p class="opacity-60">Stealer trojans were spread as harmless software...</p>
                </div>
            </div>
            <div class="footer flex items-center justify-between px-4">
                <div class="flex  gap-2 lg:gap-10">
                    <p class="opacity-60">The Mint</p>
                    <p class="opacity-60">15 mins ago</p>
                </div>
                <div class="flex gap-2 lg:gap-10">
                    <div class="flex gap-2">
                        <i class="fa-solid fa-arrow-up-from-bracket text-sky-600"></i>
                        <p class=" text-xs text-sky-600 lg:block">Share</p>
                    </div>
                    <div class="flex gap-2">
                        <i class="fa-solid fa-bookmark text-sky-600"></i>
                        <p class=" text-xs text-sky-600 lg:block">Read Late</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid gap-2 lg:gap-4 p-2 lg:p-4">
            <div class="p-2 inline-flex items-center  lg:place-items-start  gap-5 lg:gap-10">
                <img class="w-auto items-center" src="{{ asset('images/Rectangle 16.png') }}" alt="">
                <div class="grid gap-y-2">
                    <p class="text-bold text-lg">How to install Windows 11 on almost any unsupported PC
                    </p>
                    <p class="opacity-60">Microsoft is allowing some unsupported computers..</p>
                </div>
            </div>
            <div class="footer flex items-center justify-between px-4">
                <div class="flex  gap-2 lg:gap-10">
                    <p class="opacity-60">The Mint</p>
                    <p class="opacity-60">15 mins ago</p>
                </div>
                <div class="flex gap-2 lg:gap-10">
                    <div class="flex gap-2">
                        <i class="fa-solid fa-arrow-up-from-bracket text-sky-600"></i>
                        <p class=" text-xs text-sky-600 lg:block">Share</p>
                    </div>
                    <div class="flex gap-2">
                        <i class="fa-solid fa-bookmark text-sky-600"></i>
                        <p class=" text-xs text-sky-600 lg:block">Read Late</p>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="pagination flex gap-3 mx-auto items-center">
        <i class="fa-solid fa-backward-fast"></i>
        <i class="fa-solid fa-backward-step"></i>
        <div class="flex gap-2">
            <div class="border rounded py-2 px-4 hover:bg-sky-500">1</div>
            <div class="border rounded py-2 px-4 hover:bg-sky-500">2</div>
            <div class="border rounded py-2 px-4 hover:bg-sky-500 hidden lg:block">3</div>
            <div class=" rounded py-2 px-4">...</div>
            <div class="border rounded py-2 px-4 hover:bg-sky-500 hidden lg:block">8</div>
            <div class="border rounded py-2 px-4 hover:bg-sky-500">9</div>
            <div class="border rounded py-2 px-4 hover:bg-sky-500">10</div>
        </div>
        <i class="fa-solid fa-forward-step"></i>
        <i class="fa-solid fa-forward-fast"></i>
    </div>

</div>
@endsection
