@extends('layout.master')

@section('title', 'Trang chu')

@section('content')

    <div class="grid lg:mb-10 lg:mb-4 gap-y-4">
        <div
            class="grid items-center  gap-y-2 justify-between w-full p-4 bg-white rounded lg:py-4 lg:px-6 lg:p-2 lg:mb-4 lg:mb-2">
            <div class="flex justify-between items-center">
                <div class="grid gap-2 w-1/2 lg:w-3/5 lg:w-full">
                    <a href="#">
                        <p class="font-medium lg:text-lg lg:mb-4 lg:mb-2">Samsung
                            Galaxy F22 launched in India: Price, features,
                            other details</p>
                        <p class="text-lg opacity-60 lg:mb-2 lg:mb-1">Samsung Galaxy F22 has been launched in
                            India. The new smartphone has been priced in the
                            mid-range segment. The new smartphone is powered
                            by a MediaTek chipset and features a high
                            refresh rate AMOLED display.</p>

                    </a>
                </div>
                <div class="w-auto">
                    <img src="{{ asset('images/Rectangle 16.png') }}" alt="">
                </div>

            </div>

            <div class="flex justify-between lg:w-full">
                <div class="flex gap-2 lg:justify-between lg:gap-2">
                    <p class="text-xs opacity-40">The Mint</p>
                    <p class="text-xs opacity-40">15 mins ago</p>
                </div>
                <div class="flex gap-10   lg:justify-between lg:gap-2">
                    <a class="flex" href="#">
                        <img class="lg:mr-2" src="./images/icon/share.svg" alt="">
                        <p class="hidden text-xs text-sky-600 lg:block">Share</p>
                    </a>
                    <a class="flex" href="#">
                        <img class="lg:mr-2" src="./images/icon/pocket.svg" alt="">
                        <p class="hidden text-xs text-sky-600 lg:block">Read
                            Later</p>
                    </a>
                </div>
            </div>
        </div>

        <div class="grid lg:grid-cols-2 lg:w-full lg:grid lg:gap-4 lg:gap-2 lg:mb-4 lg:mb-2 gap-y-4">

            <div class="grid p-4 bg-white rounded lg:py-4 lg:px-6 lg:p-2 gap-y-2">
                <a href="news.html" class="grid p-4 bg-white rounded lg:py-4 lg:px-6 lg:p-2 gap-y-2">
                    <div class="flex items-center justify-between w-full lg:gap-4 lg:gap-2 lg:mb-4 lg:mb-2">
                        <div class="grid w-3/5 lg:w-1/2 lg:w-full gap-y-4">
                            <p class="text-lg font-medium lg:mb-4 lg:mb-2 text-slate-700">Battlegrounds
                                Mobile India iOS release date</p>
                            <p class="opacity-60">The reason behind
                                their disappointment is that iPhone
                                users have been..</p>
                        </div>
                        <div class="w-auto hidden-img">
                            <img class="" src="./images/icon/Rectangle 16.svg" alt="">
                        </div>
                    </div>
                </a>
                <div class="flex justify-between w-full">
                    <div class="flex items-center justify-between gap-4 info-hidden lg:w-1/4">
                        <p class="text-xs opacity-60">The Mint</p>
                        <p class="text-xs opacity-60">52 mins ago</p>
                    </div>
                    <div class="flex items-center justify-between gap-10   lg:w-2/5 lg:w-full ">
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/share.svg" alt="">
                            <p class="hidden text-xs text-sky-600 lg:block">Share</p>
                        </a>
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/pocket.svg" alt="">
                            <p class="hidden text-xs text-sky-600 lg:block">Read
                                Later</p>
                        </a>
                    </div>
                </div>

            </div>

            <div class="grid p-4 bg-white rounded lg:py-4 lg:px-6 lg:p-2 gap-y-2">
                <div class="flex items-center justify-between w-full lg:gap-4 lg:gap-2 lg:mb-new4 lg:mb-2">
                    <div class="grid w-3/5 lg:w-2/3 lg:3/5 gap-y-2">
                        <p class="text-lg font-medium lg:mb-4 lg:mb-2 text-slate-700">Instagram
                            working on ‘Exclusive Stories’ for
                            subscribers</p>
                        <p class="opacity-60">Instagram could be jumping on the paid subscription..</p>
                    </div>
                    <div class="w-auto hidden-img">
                        <img class="" src="./images/icon/Rectangle 16 (1).svg" alt="">
                    </div>
                </div>
                <div class="flex justify-between w-full">
                    <div class="flex items-center justify-between gap-4 info-hidden lg:w-1/4">
                        <p class="text-xs opacity-60">The Mint</p>
                        <p class="text-xs opacity-60">52 mins ago</p>
                    </div>
                    <div class="flex items-center justify-between gap-10   lg:w-2/5 lg:w-full ">
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/share.svg" alt="">
                            <p class="hidden text-xs text-sky-600 lg:block">Share</p>
                        </a>
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/pocket.svg" alt="">
                            <p class="hidden text-xs text-sky-600 lg:block">Read
                                Later</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid gap-y-4 lg:w-full lg:grid-cols-2 lg:grid lg:gap-4 lg:gap-2 lg:mb-4 lg:mb-2">
            <div class="grid p-4 bg-white rounded lg:py-4 lg:px-6 lg:p-2 gap-y-2">
                <div class="flex items-center justify-between w-full lg:gap-4 lg:gap-2 lg:mb-4 lg:mb-2">
                    <div class="grid w-full gap-y-2">
                        <p class="text-lg font-medium lg:mb-4 lg:mb-2 text-slate-700">Petrol,
                            diesel prices - July 6: Fuel prices
                            unchanged after touching record high</p>
                        <p class="opacity-60">The price of petrol
                            remained unchanged on July 6 after
                            reaching a new record high on the
                            previous day, according to a price
                            notification by state-owned..</p>
                    </div>
                </div>
                <div class="flex justify-between w-full">
                    <div class="flex items-center justify-between gap-4 info-hidden lg:w-1/4">
                        <p class="text-xs opacity-60">Radar 52</p>
                        <p class="text-xs opacity-60">1 hour ago</p>
                    </div>
                    <div class="flex items-center justify-between gap-10   lg:w-2/5 lg:w-full lg:px-2">
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/share.svg" alt="">
                            <p class="hidden text-xs text-sky-600 lg:block">Share</p>
                        </a>
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/pocket.svg" alt="">
                            <p class="hidden text-xs text-sky-600 lg:block">Read
                                Later</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="grid px-3 py-4 bg-white rounded lg:py-4 lg:px-6 lg:p-2 gap-y-2">
                <div class="flex items-center justify-between w-full lg:gap-4 lg:gap-2 lg:mb-4 lg:mb-2">
                    <div class="grid w-3/5 lg:w-2/3 lg:w-full gap-y-2">
                        <p class="text-lg font-medium lg:mb-4 lg:mb-2 text-slate-700">Android
                            smartphone users alert! Remove these 9
                            apps now</p>
                        <p class="opacity-60">Stealer trojans were
                            spread as harmless software..</p>
                    </div>
                    <div class="w-auto hidden-img">
                        <img class="" src="./images/icon/Rectangle 16 (2).svg" alt="">
                    </div>
                </div>
                <div class="flex justify-between w-full">
                    <div class="flex items-center justify-between gap-4 info-hidden lg:w-1/4">
                        <p class="text-xs opacity-60">Live Mint</p>
                        <p class="text-xs opacity-60">2 hours ago</p>
                    </div>
                    <div class="flex items-center justify-between gap-10   lg:w-2/5 lg:w-full ">
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/share.svg" alt="">
                            <p class="hidden text-xs text-sky-600 lg:block">Share</p>
                        </a>
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/pocket.svg" alt="">
                            <p class="hidden text-xs text-sky-600 lg:block">Read
                                Later</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid hidden lg:grid-cols-2 lg:w-full lg:grid lg:gap-4 lg:gap-2 lg:mb-4 lg:mb-2 gap-y-4 lg:block">
            <div class="grid bg-white rounded lg:py-4 lg:px-6 lg:p-2 gap-y-2">
                <div class="flex items-center justify-between w-full lg:gap-4 lg:gap-2 lg:mb-4 lg:mb-2">
                    <div class="grid w-3/5 lg:w-2/3 lg:w-full gap-y-2">
                        <p class="text-lg font-medium lg:mb-4 lg:mb-2 text-slate-700">How to
                            install Windows 11 on almost any
                            unsupported PC</p>
                        <p class="opacity-60">Microsoft is allowing
                            some unsupported computers..</p>
                    </div>
                    <div class="hidden-img">
                        <img class="w-auto h-auto" src="./images/windown11.png" alt="">
                    </div>
                </div>
                <div class="flex justify-between w-full">
                    <div class="flex items-center justify-between info-hidden lg:w-1/4">
                        <p class="text-xs opacity-60">Tech Mint</p>
                        <p class="text-xs opacity-60">3 hours ago</p>
                    </div>
                    <div class="flex items-center justify-between lg:w-2/5 lg:w-full ">
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/share.svg" alt="">
                            <p class="text-xs text-sky-600">Share</p>
                        </a>
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/pocket.svg" alt="">
                            <p class="text-xs text-sky-600">Read
                                Later</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="bg-white rounded lg:py-4 lg:px-6 lg:p-2">
                <div class="flex items-center justify-between w-full lg:gap-4 lg:gap-2 lg:mb-4 lg:mb-2">
                    <div class="lg:w-2/3 lg:w-full">
                        <p class="text-lg font-medium lg:mb-4 lg:mb-2 text-slate-700">This
                            charging feature from Android phones may
                            come to iPhone 13</p>
                        <p class="opacity-60">You will always find a
                            set of people who will say that..</p>
                    </div>
                    <div class="hidden-img">
                        <img class="w-auto h-auto" src="./images/ip13.png" alt="">
                    </div>
                </div>
                <div class="flex justify-between w-full">
                    <div class="flex items-center justify-between info-hidden lg:w-1/4">
                        <p class="text-xs opacity-60">Live Mint</p>
                        <p class="text-xs opacity-60">5 hours ago</p>
                    </div>
                    <div class="flex items-center justify-between lg:w-2/5 lg:w-full ">
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/share.svg" alt="">
                            <p class="text-xs text-sky-600">Share</p>
                        </a>
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/pocket.svg" alt="">
                            <p class="text-xs text-sky-600">Read
                                Later</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="grid lg:mb-10 lg:mb-4 gap-y-4">
        <div class="flex justify-between w-full lg:mb-4 lg:mb-2">
            <div class="flex lg:gap-6 lg:gap-2">
                <img src="./images/icon/feather.svg" alt="">
                <p class="text-base font-bold">Creators you should follow</p>
            </div>
            <div class="flex gap-4 lg:gap-5 lg:gap-2">
                <div>
                    <img src="./images/icon/arrow-right (1).svg" alt="">
                </div>
                <div>
                    <img src="./images/icon/arrow-right.svg" alt="">
                </div>
            </div>
        </div>
        <div class="max-w-xs sm:max-w-xl lg:max-w-6xl lg:gap-4 lg:flex slick-img">
            <div class="relative p-2 m-4 bg-white rounded lg:h-full lg:p-4 lg:p-1 lg:col-span-1 img-1">
                <div class="mb-2">
                    <img class="mx-auto " src="./images/taylor.png" alt="">
                </div>
                <div class="mb-4 text-center">
                    <p class="mb-1 text-lg font-medium text-slate-800">Shan Teylor</p>
                    <p class="text-xs font-normal text-slate-800 opacity-60">Tech Mint</p>
                </div>
                <div class="absolute w-10/12 py-4 mt-auto bg-blue-500 rounded lg:static lg:w-full left-3 bottom-2 lg:py-2">
                    <p class="text-center text-white">Follow</p>
                </div>
            </div>
            <div class="relative p-2 m-4 bg-white rounded lg:p-4 lg:p-1 img-1">
                <div class="mb-2">
                    <img class="mx-auto " src="./images/angela.png" alt="">
                </div>
                <div class="mb-4 text-center">
                    <p class="mb-1 text-lg font-medium text-slate-800">Mary Angela</p>
                    <p class="text-xs font-normal text-slate-800 opacity-60">Live Mint</p>
                </div>
                <div class="absolute w-10/12 py-4 mt-auto bg-blue-500 rounded lg:static lg:w-full left-3 bottom-2 lg:py-2">
                    <p class="text-center text-white">Follow</p>
                </div>
            </div>
            <div class="relative p-2 m-4 bg-white rounded lg:p-4 lg:p-1 img-1">
                <div class="mb-2">
                    <img class="mx-auto " src="./images/chi.png" alt="">
                </div>
                <div class="mb-4 text-center">
                    <p class="mb-1 text-lg font-medium text-slate-800">Kyon Cho Chi</p>
                    <p class="text-xs font-normal text-slate-800 opacity-60">Bizz Daily</p>
                </div>
                <div class="absolute w-10/12 py-4 mt-auto bg-blue-500 rounded lg:static lg:w-full left-3 bottom-2 lg:py-2">
                    <p class="text-center text-white">Follow</p>
                </div>
            </div>
            <div class="p-2 m-4 bg-white rounded lg:p-4 lg:p-1 img-1">
                <div class="mb-2">
                    <img class="mx-auto " src="./images/livingstone.png" alt="">
                </div>
                <div class="mb-4 text-center">
                    <p class="mb-1 text-lg font-medium text-slate-800">Paul Livingstone</p>
                    <p class="text-xs font-normal text-slate-800 opacity-60">Sport Biz</p>
                </div>
                <div class="py-4 mt-auto bg-blue-500 rounded lg:static lg:w-full left-3 bottom-2 lg:py-2">
                    <p class="text-center text-white">Follow</p>
                </div>
            </div>
            <div class="relative p-2 m-4 bg-white rounded lg:p-4 lg:p-1 img-1">
                <div class="mb-2">
                    <img class="mx-auto " src="./images/jay.png" alt="">
                </div>
                <div class="mb-4 text-center">
                    <p class="mb-1 text-lg font-medium text-slate-800">Sara Jay</p>
                    <p class="text-xs font-normal text-slate-800 opacity-60">Radar 52</p>
                </div>
                <div class="absolute w-10/12 py-4 mt-auto bg-blue-500 rounded lg:static lg:w-full left-3 bottom-2 lg:py-2">
                    <p class="text-center text-white">Follow</p>
                </div>
            </div>
        </div>
    </div>
    <div class="grid gap-y-4">
        <div class="grid lg:grid-cols-2 lg:w-full lg:gap-4 lg:gap-2 lg:mb-4 lg:mb-2 gap-y-4">
            <div class="grid px-3 py-4 bg-white rounded lg:py-4 lg:px-6 lg:p-2 gap-y-2 ">
                <div class="flex items-center justify-between w-full lg:gap-4 lg:gap-2 lg:mb-4 lg:mb-2">
                    <div class="grid w-3/5 lg:w-2/3 lg:w-full gap-y-2">
                        <p class="text-lg font-medium lg:mb-4 lg:mb-2 text-slate-700">INDvENG
                            Tests to be played in front of crowd
                        </p>
                        <p class="opacity-60">The 5-Test series
                            between India & England is set to be
                            played in front of packed..</p>
                    </div>
                    <div class="w-auto hidden-img">
                        <img class="" src="./images/icon/Rectangle 16 (3).svg" alt="">
                    </div>
                </div>
                <div class="flex justify-between w-full">
                    <div class="flex items-center justify-between gap-4 info-hidden lg:w-1/4">
                        <p class="text-xs opacity-60">Sport Biz</p>
                        <p class="text-xs opacity-60">7 hours ago</p>
                    </div>
                    <div class="flex items-center justify-between gap-10   lg:w-2/5 lg:w-full ">
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/share.svg" alt="">
                            <p class="hidden text-xs lg:block text-sky-600">Share</p>
                        </a>
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/pocket.svg" alt="">
                            <p class="hidden text-xs text-sky-600 lg:block">Read
                                Later</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="grid px-3 py-4 bg-white rounded lg:py-4 lg:px-6 lg:p-2 gap-y-2">
                <div class="flex items-center justify-between w-full lg:gap-4 lg:gap-2 lg:mb-4 lg:mb-2">
                    <div class="grid w-3/5 lg:w-2/3 lg:w-full gap-y-2">
                        <p class="text-lg font-medium lg:mb-4 lg:mb-2 text-slate-700">The 10
                            Coolest Wearable Tech Gadgets Of 2021
                            (So Far)</p>
                        <p class="opacity-60">Driven by demand for
                            connected earbuds and a..</p>
                    </div>
                    <div class="hidden-img">
                        <img class="w-auto h-auto" src="./images/watch.png" alt="">
                    </div>
                </div>
                <div class="flex justify-between w-full">
                    <div class="flex items-center justify-between gap-4 info-hidden lg:w-1/4">
                        <p class="text-xs opacity-60">The Mint</p>
                        <p class="text-xs opacity-60">7 hours ago</p>
                    </div>
                    <div class="flex items-center justify-between gap-10 px-4 lg:w-2/5 lg:w-full ">
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/share.svg" alt="">
                            <p class="hidden text-xs lg:block text-sky-600">Share</p>
                        </a>
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/pocket.svg" alt="">
                            <p class="hidden text-xs lg:block text-sky-600">Read
                                Later</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid lg:grid-cols-2 lg:w-full lg:grid lg:gap-4 lg:gap-2 lg:mb-4 lg:mb-2 gap-y-4 ">
            <div class="grid px-3 py-4 bg-white rounded lg:py-4 lg:px-6 lg:p-2 gap-y-2">
                <div class="flex items-center justify-between w-full lg:gap-4 lg:gap-2 lg:mb-4 lg:mb-2">
                    <div class="grid w-full gap-y-2">
                        <p class="text-lg font-medium lg:mb-4 lg:mb-2 text-slate-700">Biden's cold
                            response to Afghanistan's collapse to
                            have far consequences
                        </p>
                        <p class="opacity-60">When US President Joe
                            Biden chose in April to withdraw all
                            American forces from Afghanistan by
                            September, we were among those who
                            judged..</p>
                    </div>
                </div>
                <div class="flex justify-between w-full">
                    <div class="flex items-center justify-between gap-4 info-hidden lg:w-1/4">
                        <p class="text-xs opacity-60">Radar 52</p>
                        <p class="text-xs opacity-60">8 hours ago</p>
                    </div>
                    <div class="flex items-center justify-between gap-10   lg:w-2/5 lg:w-full ">
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/share.svg" alt="">
                            <p class="hidden text-xs text-sky-600 lg:block">Share</p>
                        </a>
                        <a class="flex lg:w-1/2" href="#">
                            <img class="lg:mr-2" src="./images/icon/pocket.svg" alt="">
                            <p class="hidden text-xs text-sky-600 lg:block">Read
                                Later</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="grid px-3 py-4 bg-white rounded gap-y-2 lg:py-4 lg:px-6 lg:p-2">
                <div class="flex items-center justify-between w-full lg:gap-4 lg:gap-2 lg:mb-4 lg:mb-2">
                    <div class="w-3/5 lg:w-2/3 lg:w-full">
                        <p class="text-lg font-medium lg:mb-4 lg:mb-2 text-slate-700">Hardik
                            Pandya 'is bowling and it is a very good
                            sign' - Suryakumar</p>
                        <p class="opacity-60">I think that the team
                            management and Hardik..</p>
                    </div>
                    <div class="hidden-img">
                        <img class="w-auto h-auto" src="./images/pandya.png" alt="">
                    </div>
                </div>
                <div class="flex justify-between w-full">
                    <div class="flex items-center justify-between gap-4 info-hidden lg:w-1/4">
                        <p class="text-xs opacity-60">Cric Mint</p>
                        <p class="text-xs opacity-60">9 hours ago</p>
                    </div>
                    <div class="inline-flex items-center justify-between gap-10   lg:w-2/5 lg:w-full ">
                        <a class="flex" href="#">
                            <img class="lg:mr-2" src="./images/icon/share.svg" alt="">
                            <p class="hidden text-xs text-sky-600 lg:block">Share</p>
                        </a>
                        <a class="flex" href="#">
                            <img class="lg:mr-2" src="./images/icon/pocket.svg" alt="">
                            <p class="hidden text-xs text-sky-600 lg:block">Read
                                Later</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex lg:mt-16 lg:mt-4">
            <div class="px-4 py-2 mx-auto border rounded">
                <p class="text-slate-800">Show More</p>
            </div>
        </div>
    </div>
    </div>
@endsection
