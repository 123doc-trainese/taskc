function showModal() {
    var modal = document.getElementById('modal');
    var overlay = document.getElementById('overlay');
    modal.classList.remove('hidden');
    overlay.classList.remove('hidden');
}

function off() {
    var modal = document.getElementById('modal');
    var overlay = document.getElementById('overlay');
    modal.classList.add('hidden');
    overlay.classList.add('hidden');
}


function down() {
    document.getElementById("arrow-down").hidden = true;
    document.getElementById("arrow-up").hidden = false;
    var a = document.getElementById("comment-hidden");
    a.classList.remove('hidden');

}

function up() {
    document.getElementById("arrow-down").hidden = false;
    document.getElementById("arrow-up").hidden = true;
    var a = document.getElementById("comment-hidden");
    a.classList.add('hidden');
}

$(document).ready(function() {

    $('.slick-img').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        infinite: false,
        responsive: [{
                breakpoint: 400,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    // centerMode: true,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('.menu-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        infinite: false,
        responsive: [{
                breakpoint: 400,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    centerMode: false,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    centerMode: false,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    centerMode: false,
                }
            }
        ]
    });
});