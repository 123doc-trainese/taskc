<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::get('/login', [LoginController::class, 'login']);

Route::post('/login', [LoginController::class, 'checklogin'])->name('login');

Route::get('/register', [LoginController::class, 'register'])->name('register');

Route::post('/registerUser', [LoginController::class, 'regsiterUser'])->name('registerUser');

Route::get('/category/{id}', [CategoryController::class, 'getCategoryById'])->name('category.android');